currencies = {'japan' =>'yen', 'us' => 'dollar', 'india' => 'rupee'}

currencies['japan'] #=>"yen"

currencies[:japan] #=>"yen"



'apple'.methods    #文字列のメソッドを返す
=> [:include?, :%, :unicode_normalize, :*, :+, :to_c, :unicode_normalize!, :unicode_normalized?, :count, :partition, :unpack, :encode, :encode!, :next, :casecmp, :insert, :bytesize, :match, :succ!, :next!, :upto, :index, :rindex, :replace, :clear, :chr, :+@, :-@, :setbyte, :getbyte, :<=>, :<<, :scrub, :scrub!, :byteslice, :==, :===, :dump, :=~, :downcase, :[], :[]=, :upcase, :downcase!, :capitalize, :swapcase, :upcase!, :oct, :empty?, :eql?, :hex, :chars, :split, :capitalize!, :swapcase!, :concat, :codepoints, :reverse, :lines, :bytes, :prepend, :scan, :ord, :reverse!, :center, :sub, :freeze, :inspect, :intern, :end_with?, :gsub, :chop, :crypt, :gsub!, :start_with?, :rstrip, :sub!, :ljust, :length, :size, :strip!, :succ, :rstrip!, :chomp, :strip, :rjust, :lstrip!, :tr!, :chomp!, :squeeze, :lstrip, :tr_s!, :to_str, :to_sym, :chop!, :each_byte, :each_char, :each_codepoint, :to_s, :to_i, :tr_s, :delete, :encoding, :force_encoding, :sum, :delete!, :squeeze!, :tr, :to_f, :valid_encoding?, :slice, :slice!, :rpartition, :each_line, :b, :ascii_only?, :to_r, :hash, :<, :>, :<=, :>=, :between?, :instance_of?, :public_send, :instance_variable_get, :instance_variable_set, :instance_variable_defined?, :remove_instance_variable, :private_methods, :kind_of?, :instance_variables, :tap, :define_singleton_method, :is_a?, :public_method, :extend, :singleton_method, :to_enum, :enum_for, :!~, :respond_to?, :display, :object_id, :send, :method, :nil?, :class, :singleton_class, :clone, :dup, :itself, :taint, :tainted?, :untaint, :untrust, :trust, :untrusted?, :methods, :protected_methods, :frozen?, :public_methods, :singleton_methods, :!, :!=, :__send__, :equal?, :instance_eval, :instance_exec, :__id__]

:apple.methods    #リテラルのメソッドを返す
=> [:<=>, :==, :===, :=~, :[], :empty?, :inspect, :intern, :length, :size, :succ, :to_sym, :to_proc, :to_s, :next, :casecmp, :match, :upcase, :downcase, :capitalize, :swapcase, :slice, :encoding, :id2name, :<, :>, :<=, :>=, :between?, :instance_of?, :public_send, :instance_variable_get, :instance_variable_set, :instance_variable_defined?, :remove_instance_variable, :private_methods, :kind_of?, :instance_variables, :tap, :define_singleton_method, :is_a?, :public_method, :extend, :singleton_method, :to_enum, :enum_for, :!~, :eql?, :respond_to?, :freeze, :display, :object_id, :send, :method, :nil?, :hash, :class, :singleton_class, :clone, :dup, :itself, :taint, :tainted?, :untaint, :untrust, :trust, :untrusted?, :methods, :protected_methods, :frozen?, :public_methods, :singleton_methods, :!, :!=, :__send__, :equal?, :instance_eval, :instance_exec, :__id__]


currencies = {:japan =>'yen', :us => 'dollar', :india => 'rupee'}
#ハッシュのキーをシンボルにする
currencies[:us] #=>"dollar"

currencies[:italy] = 'euro'


currencies = {japan: 'yen', us: 'dollar', india: 'rupee'}
#=> ではなくシンボル: '' の記法で作成した
currencies[:us] #=>"dollar"


currencies = {japan: :yen, us: :dollar, india: :rupee}
#キーの値も:で表す

currencies = {:japan=> :yen, :us=> :dollar, :india=> :rupee}
#キーと値をシンボルで表す
